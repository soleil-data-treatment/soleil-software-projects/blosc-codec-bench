CONFIG ?= config.mk
include $(CONFIG)

SOURCE := main.c
EXEC := $(SOURCE:.c=.exe)

SOURCES := $(wildcard src/*.c)
OBJECTS := $(SOURCES:.c=.o)

INCLUDES := -I$(CURDIR)/src -I$(BLOSC_INC)
LDFLAGS := -L$(BLOSC_LIB)
LDLIBS := -l$(BLOSC_LIBNAME)

.PHONY: all debug check clean distclean

all: $(EXEC)

$(EXEC): $(OBJECTS)
	@echo "Do not forget to set the LB_LIBRARY_PATH environment variable:"
	@echo export LD_LIBRARY_PATH=$(BLOSC_LIB)
	$(CC) $(CFLAGS) -o $@ $^ $(LDFLAGS) $(LDLIBS)

src/%.o: src/%.c
	$(CC) $(CFLAGS) $(INCLUDES) -c $^ -o $@

debug: CFLAGS += -DDEBUG
debug: $(EXEC)

check:
	@echo using $(CONFIG)
	$(MAKE) -C $(TEST_DIR)
	$(MAKE) -C $(TEST_DIR) run
	$(eval OUT_DIR = $(shell mktemp -d))
	./$(EXEC) $(TEST_DIR)/$(TEST_BIN) lz4 -o $(OUT_DIR) -c 256
	du -sb $(TEST_DIR)/$(TEST_BIN) $(OUT_DIR)/out_d.bin $(OUT_DIR)/out_c.bin
	diff $(TEST_DIR)/$(TEST_BIN) $(OUT_DIR)/out_d.bin
	if [ $$? -eq 0 ]; then echo "OK: binary files are identical"; $(RMDIR) $(OUT_DIR); fi

clean:
	$(RM) src/*.o
	$(MAKE) -C $(TEST_DIR) clean

distclean:
	$(RM) src/*.o *.exe
	$(MAKE) -C $(TEST_DIR) distclean
