# Blosc Codec Bench

A script to run performance tests on 32-bit floating-point binary files using c-blosc2.

The script returns a CSV output with the following fields:
- the compressor fullname (name, version, level of compression and the number of threads)
- the compression speed in MB/s
- the decompression speed in MB/s
- the input file size
- the compressed file size
- the compression ratio
- the filename

The supported compressors are: `blosclz`, `lz4`, `lz4hc`, `zlib`, `zstd`.

# Compilation
```
make
make check
make clean
```

# Usage
```
./main.exe -h
```

Basic usage using the input binary file `infile`(defined as a bash variable) with the compressor `zstd`:
```
./main.exe $infile zstd
```
It is possible to specify the chunk length (default value is 256, making a chunk size of 4 * 256 = 1024 bytes):
```
./main.exe $infile zstd -c 10000
```
or the compression level (here 2, default value is 1):
```
./main.exe $infile zstd -l 2
```
or the number of threads (here 4, default value is 8):
```
./main.exe $infile zstd -p 4
```
Bitshuffle can be activated (use `-s` for byte-shuffle):
```
./main.exe $infile zstd -b
```
A temporay output directory, where both compressed and decompressed files are written, can be defined (default value is /tmp):
```
./main.exe $infile zstd -o /tmp/toto
```
And of course, all these options can be combined.
