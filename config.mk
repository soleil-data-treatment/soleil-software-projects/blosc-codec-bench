BLOSC_INC := /opt/c-blosc2/include
BLOSC_LIB := /opt/c-blosc2/lib
BLOSC_LIBNAME := blosc2

CC := gcc

CFLAGS := -O -Wall

TEST_DIR := example
TEST_BIN := test-float.bin

RM := rm -f
RMDIR := rm -r