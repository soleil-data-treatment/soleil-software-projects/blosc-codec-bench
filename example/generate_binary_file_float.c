#include <stdio.h>
#include <stdlib.h>
#include <errno.h>

//#define CHUNK_LEN (1000 * 1000)
#define CHUNK_LEN 64
#define NB_CHUNKS 16


int main(int argc, char* argv[argc+1])
{
    char* filename = "test-float.bin";

    /* Write a dummy binary file that will be used to test compression and decompression using c-blosc2 */
    errno = 0;
    FILE* outfile = fopen(filename, "wb");
    if (!outfile) {
        perror("fopen failed");
        return EXIT_FAILURE;
    }

    for (size_t chunk_no = 0; chunk_no < NB_CHUNKS; ++chunk_no) {
        static float buffer[CHUNK_LEN];
        for (int i = 0; i < CHUNK_LEN; ++i) {
            buffer[i] = chunk_no + 1;
        }

        fwrite(buffer, 1, sizeof(buffer), outfile);   
    }
    
    fclose(outfile);

    /* Check that I can read the biary file correctly */
    errno = 0;
    FILE* infile = fopen(filename, "rb");
    if (!infile) {
        perror("fopen failed");
        return EXIT_FAILURE;
    }

    for (size_t chunk_no = 0; chunk_no < NB_CHUNKS; ++chunk_no) {
        // printf("- chunk #%ld\n", chunk_no);
        static float data[CHUNK_LEN];
        size_t data_size = sizeof(data);
        size_t read_size = fread(data, 1, data_size, infile);
        if (read_size != data_size) {
            fprintf(stderr, "Issue with read_size: %ld (should be %ld)\n", read_size, data_size);
        }
        /* Check that I do retrieve the corresct value */
        // for (int i = 0; i < CHUNK_LEN; ++i) {
        //     printf("    data[%d] = %0.2f\n", i, data[i]);
        // }
    }
    fclose(infile);

    return EXIT_SUCCESS;
}