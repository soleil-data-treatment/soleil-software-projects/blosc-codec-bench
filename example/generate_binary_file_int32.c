#include <stdio.h>
#include <stdlib.h>
#include <errno.h>

//#define CHUNK_LEN (1000 * 1000)
#define CHUNK_LEN 64
#define NB_CHUNKS 16
# define FULL_LEN (CHUNK_LEN * NB_CHUNKS)

int main(int argc, char* argv[argc+1])
{
    char* filename = "test-int32.bin";

    /* Write a dummy binary file that will be used to test compression and decompression using c-blosc2 */
    errno = 0;
    FILE* outfile = fopen(filename, "wb");
    if (!outfile) {
        perror("fopen failed");
        return EXIT_FAILURE;
    }

    for (size_t chunk_no = 0; chunk_no < NB_CHUNKS; ++chunk_no) {
        static int32_t buffer[CHUNK_LEN];
        for (int i = 0; i < CHUNK_LEN; ++i) {
            buffer[i] = chunk_no + 1;
        }

        fwrite(buffer, 1, sizeof(buffer), outfile);   
    }
    
    fclose(outfile);

    /* Check that I can read the binary file correctly */
    errno = 0;
    FILE* infile = fopen(filename, "rb");
    if (!infile) {
        perror("fopen failed");
        return EXIT_FAILURE;
    }

    for (size_t chunk_no = 0; chunk_no < NB_CHUNKS; ++chunk_no) {
        // printf("- chunk #%ld\n", chunk_no);
        static int32_t data[CHUNK_LEN];
        size_t data_size = sizeof(data);
        size_t read_size = fread(data, 1, data_size, infile);
        if (read_size != data_size) {
            fprintf(stderr, "Issue with read_size: %ld (should be %ld)\n", read_size, data_size);
        }
        /* Check that I do retrieve the correct values */
        // for (int i = 0; i < CHUNK_LEN; ++i) {
        //     printf("    data[%d] = %d\n", i, data[i]);
        // }
    }
    fclose(infile);

    /* Read all the chunks all at once */
    errno = 0;
    FILE* infile2 = fopen(filename, "rb");
    if (!infile2) {
        perror("fopen failed");
        return EXIT_FAILURE;
    }

    static int32_t all_data[FULL_LEN];
    size_t all_data_size = sizeof(all_data);
    size_t read_size = fread(all_data, 1, all_data_size, infile);
    if (read_size != sizeof(all_data)) {
        fprintf(stderr, "Issue with read_size: %ld (should be %ld)\n", read_size, all_data_size);
    }
    /* Check that I do retrieve the correct values */
    // for (int i = 0; i < FULL_LEN; ++i) {
    //     printf("all_data[%d] = %d\n", i, all_data[i]);
    // }
    fclose(infile2);

    return EXIT_SUCCESS;
}