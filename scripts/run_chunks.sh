#/bin/bash
#
# Return the de\compression performances of a compressor for several chunk lengths"

CSV_HEADER="CSV_HEADER.txt"

if [  $# -lt 2 ]; then
    echo "Usage: $0 INFILE COMPRESSOR"
    exit 1
fi

INFILE=$1
COMPRESSOR=$2

CHUNK_LENGTHS="10000 100000 1000000 10000000"

cat ${CSV_HEADER}
for chunk_len in ${CHUNK_LENGTHS}; do
    tmpdir=$(mktemp -d)
    ./../main.exe $INFILE $COMPRESSOR -c ${chunk_len} -o ${tmpdir}
    [ -d "${tmpdir}" ] && rm -r ${tmpdir}
done
