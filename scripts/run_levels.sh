#/bin/bash
#
# Return the de\compression performances of a compressor for several compression levels"

CSV_HEADER="CSV_HEADER.txt"

if [  $# -lt 2 ]; then
    echo "Usage: $0 INFILE COMPRESSOR [OPTION]"
    echo "  OPTION: -b (bitshuffle) or -s (shuffle) [default: empty]"
    exit 1
fi

INFILE=$1
COMPRESSOR=$2
OPTION=$3

if [ "${COMPRESSOR}" = "lz4" ]; then
    LEVELS="1 5 9"
elif [ "${COMPRESSOR}" = "zstd" ]; then 
    LEVELS="1 5 9 10 15 19"
else
    echo "Unknown compressor: ${COMPRESSOR}"
    exit
fi

cat ${CSV_HEADER}
for level in ${LEVELS}; do
    tmpdir=$(mktemp -d)
    ./../main.exe $INFILE $COMPRESSOR -l ${level} -o ${tmpdir} ${OPTION}
    [ -d "${tmpdir}" ] && rm -r ${tmpdir}
done
