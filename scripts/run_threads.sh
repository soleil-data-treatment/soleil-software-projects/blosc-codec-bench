#/bin/bash
#
# Return the de\compression performances of a compressor for several number of threads"

CSV_HEADER="CSV_HEADER.txt"

if [  $# -lt 2 ]; then
    echo "Usage: $0 INFILE COMPRESSOR"
    exit 1
fi

INFILE=$1
COMPRESSOR=$2

THREADS_LIST="1 2 4 8 16 32 64 128"

cat ${CSV_HEADER}
for threads in ${THREADS_LIST}; do
    tmpdir=$(mktemp -d)
    ./../main.exe $INFILE $COMPRESSOR -p ${threads} -o ${tmpdir}
    [ -d "${tmpdir}" ] && rm -r ${tmpdir}
done
