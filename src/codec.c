#include "codec.h"

char* codecs[] = {"blosclz", "lz4", "lz4hc", "zlib", "zstd"};
int nb_codecs = sizeof(codecs) / sizeof(codecs[0]);

bool codec_exists(char* codec) {
    bool found = false;
    for(int i = 0; i < nb_codecs; ++i) {
        if (strcmp(codecs[i], codec) == 0)
            found = true;
    }

    if (!found)
        fprintf(stderr, "Compressor does not exist: %s\n", codec);

    return found;
}

void set_codec_version(char* codec, char* version) {
    if (strcmp(codec, "blosclz") == 0) {
        strcpy(version, CODEC_VERSION_BLOSCLZ);
    } else if (strcmp(codec, "lz4") == 0) {
        strcpy(version, CODEC_VERSION_LZ4);
    } else if (strcmp(codec, "lz4hc") == 0) {
        strcpy(version, CODEC_VERSION_LZ4);
    } else if (strcmp(codec, "zlib") == 0) {
        strcpy(version, CODEC_VERSION_ZLIB);
    } else if (strcmp(codec, "zstd") == 0) {
        strcpy(version, CODEC_VERSION_ZSTD);
    } else {
        fprintf(stderr, "Unknown compressor\n");
        exit(EXIT_FAILURE);
    }
}