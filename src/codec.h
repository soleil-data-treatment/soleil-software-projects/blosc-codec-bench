#pragma once

#include "common.h"

#define VERSION_LENGTH_MAX 20
#define CODEC_VERSION_BLOSCLZ "2.5.1"
#define CODEC_VERSION_LZ4 "1.9.3"
#define CODEC_VERSION_ZLIB "2.0.6"
#define CODEC_VERSION_ZSTD "1.5.2"

extern int nb_codecs;
extern char* codecs[];

bool codec_exists(char*);
void set_codec_version(char*, char*);
