#pragma once

#include <ctype.h>
#include <errno.h>
#include <limits.h>  /* for PATH_MAX */
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#define STRINGIFY(X) #X
#define STRGY(X) STRINGIFY(X)

#ifdef DEBUG
# define TRACE_ON 1
#else
# define TRACE_ON 0
#endif

#define debug_print(F, ...)                                                                          \
do {                                                                                                 \
    if (TRACE_ON)                                                                                    \
        fprintf(stderr, "DEBUG -- %s, %s:" STRGY(__LINE__) ": " F, __FILE__, __func__, __VA_ARGS__); \
} while (false)
