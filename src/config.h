#pragma once

#include "common.h"

#define OPTION_BITSHUFFLE_FLAG false
#define OPTION_CHUNK_LEN 1048576
#define OPTION_LEVEL 1
#define OPTION_NTHREADS 8
#define OPTION_OUTDIR "/tmp"
#define OPTION_SHUFFLE_FLAG false