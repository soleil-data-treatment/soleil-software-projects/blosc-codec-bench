#include <blosc2.h>

#include "common.h"
#include "config.h"
#include "constants.h"
#include "codec.h"
#include "option.h"
#include "utils.h"


int main(int argc, char* argv[argc+1]) {

    option o = option_get(argc, argv);

    blosc_init();


    int64_t nbytes, cbytes;
    blosc_timestamp_t last, current;
    double duration_c, duration_d;

    debug_print("Blosc version info: %s (%s)\n", BLOSC_VERSION_STRING, BLOSC_VERSION_DATE);

    /* Create a super-chunk container */
    blosc2_cparams cparams = BLOSC2_CPARAMS_DEFAULTS;
    cparams.typesize = sizeof(int32_t);
    cparams.clevel = o.level;
    cparams.nthreads = o.nthreads;

    if (strcmp(o.codec, "blosclz") == 0) {
        cparams.compcode = BLOSC_BLOSCLZ;
    } else if (strcmp(o.codec, "lz4") == 0) {
        cparams.compcode = BLOSC_LZ4;
    } else if (strcmp(o.codec, "lz4hc") == 0) {
        cparams.compcode = BLOSC_LZ4HC;
    } else if (strcmp(o.codec, "zlib") == 0) {
        cparams.compcode = BLOSC_ZLIB;
    } else if (strcmp(o.codec, "zstd") == 0) {
        cparams.compcode = BLOSC_ZSTD;
    } else {
        fprintf(stderr, "Unknown compressor\n");
        return EXIT_FAILURE;
    }

    if (o.shuffle_flag) {
        cparams.filters[BLOSC2_MAX_FILTERS - 1] = BLOSC_SHUFFLE;
    }

    if (o.bitshuffle_flag) {
        cparams.filters[BLOSC2_MAX_FILTERS - 1] = BLOSC_BITSHUFFLE;
    }

    blosc2_dparams dparams = BLOSC2_DPARAMS_DEFAULTS;
    dparams.nthreads = o.nthreads;

    /* Create a super-chunk backed by an in-memory frame */
    char filename_c[PATH_MAX];
    strcpy(filename_c, o.outdir);
    strcat(filename_c, "/");
    strcat(filename_c, "out_c.bin");

    remove(filename_c);
    blosc2_storage storage = {.cparams=&cparams, .dparams=&dparams, .contiguous=true, .urlpath=filename_c};
    blosc2_schunk* schunk = blosc2_schunk_new(&storage);

    // Compress the file
    blosc_set_timestamp(&last);

    FILE* infile = fopen(o.filename, "rb");
    if (!infile) {
        fprintf(stderr, "Input file cannot be open.");
        return EXIT_FAILURE;
    }

    long infile_size = get_binfile_size(infile);
    debug_print("Input file size (bytes) = %ld\n", infile_size);

    size_t data_size = o.chunk_len * sizeof(float);  // should be explicit because sizeof(data) will not work

    debug_print("data size of a chunk (bytes) = %ld\n", data_size);

    if (data_size > infile_size) {
        fprintf(stderr, "The chunk data size (%ld bytes) should be less than the input file size (%ld bytes)!\n", data_size, infile_size);
        return EXIT_FAILURE;
    }

    errno = 0;
    float* data = malloc(data_size);
    if (!data) {
        perror("malloc failed on data");
        return EXIT_FAILURE;
    }

    size_t read_size;
    int64_t chunk_no = -1;

    while (true) {
        read_size = fread(data, 1, data_size, infile);

        debug_print("read_size (bytes) = %ld\n", read_size);

        if (read_size < 0) {
            fprintf(stderr, "Error in reading data from input file");
            return EXIT_FAILURE;
        }

        chunk_no = blosc2_schunk_append_buffer(schunk, data, read_size);

        debug_print("chunk_no = %ld\n", chunk_no);

        if (chunk_no < 0) {
            fprintf(stderr, "Error in appending data to destination file");
            return EXIT_FAILURE;
        }

        if (read_size != data_size)
            break;

    }

    fclose(infile);

    int64_t nb_chunks = chunk_no - 1;

    debug_print("Total size (bytes) = %ld\n", o.chunk_len * nb_chunks * sizeof(float) + read_size);


    /* Gather some info */
    nbytes = schunk->nbytes;
    cbytes = schunk->cbytes;
    blosc_set_timestamp(&current);
    duration_c = blosc_elapsed_secs(last, current);

    float raw_size = (float)nbytes / MB; // in MB
    float compressed_size = (float)cbytes / MB;  // in MB
    float ratio = (1. * (float)nbytes) / (float)cbytes;
    float cspeed = (float)nbytes / (duration_c * MB);  // in MB/s

    debug_print("Compression ratio: %.1f MB -> %.1f MB (%.1fx)\n", raw_size, compressed_size, ratio);
    debug_print("Compression time: %.3g s, %.1f MB/s\n", duration_c, cspeed);

    // Decompressing the chunks
    char filename_d[PATH_MAX];
    strcpy(filename_d, o.outdir);
    strcat(filename_d, "/");
    strcat(filename_d, "out_d.bin");

    remove(filename_d);

    blosc_set_timestamp(&last);

    FILE* outfile = fopen(filename_d,"wb");
    if (!outfile) {
        fprintf(stderr, "Output file cannot be open.");
        return EXIT_FAILURE;
    }

    errno = 0;
    float* data_dest = malloc(data_size);
    if (!data_dest) {
        perror("malloc failed on data_dest");
        return EXIT_FAILURE;
    }

    for (int j = 0; j < nb_chunks; ++j) {
        int dsize = blosc2_schunk_decompress_chunk(schunk, j, data_dest, data_size);
        if (dsize < 0) {
            fprintf(stderr,"Decompression error. Error code: %d\n", dsize);
            return EXIT_FAILURE;
        }
        fwrite(data_dest, 1, data_size, outfile);
    }

    /* Deal with the last chunk */
    if (read_size > 0) {
        int dsize = blosc2_schunk_decompress_chunk(schunk, nb_chunks, data_dest, data_size);
        if (dsize < 0) {
            fprintf(stderr,"Decompression error. Error code: %d\n", dsize);
            return EXIT_FAILURE;
        }
        /* The  last decompressed chunk has to be shortened to the correct length */
        const int last_chunk_len = read_size / sizeof(float);
        float last_data_dest[last_chunk_len];
        for (int k = 0; k < last_chunk_len; ++k) {
            last_data_dest[k] = data_dest[k];
        }
        fwrite(last_data_dest, 1, sizeof(last_data_dest), outfile);
    }

    fclose(outfile);

    blosc_set_timestamp(&current);
    duration_d = blosc_elapsed_secs(last, current);
    float dspeed = (float)nbytes / (duration_d * MB);  // in MB/s

    debug_print("Decompression time: %.3g s, %.1f MB/s\n", duration_d, dspeed);

    // Print final CSV-like results to stdout
    char codec_version[VERSION_LENGTH_MAX];
    set_codec_version(o.codec, codec_version);

    char compressor[0x100];
    if (o.bitshuffle_flag) {
        snprintf(compressor, sizeof(compressor), "blosc-%s-bs %s-%s -%d -p%d -c%d", o.codec, BLOSC_VERSION_STRING, codec_version, o.level, o.nthreads, o.chunk_len);
    }
    else if (o.shuffle_flag) {
        snprintf(compressor, sizeof(compressor), "blosc-%s-s %s-%s -%d -p%d -c%d", o.codec, BLOSC_VERSION_STRING, codec_version, o.level, o.nthreads, o.chunk_len);
    } else {
        snprintf(compressor, sizeof(compressor), "blosc-%s %s-%s -%d -p%d -c%d", o.codec, BLOSC_VERSION_STRING, codec_version, o.level, o.nthreads, o.chunk_len);
    }

    printf("%s,%.1f,%.1f,%ld,%ld,%.2f,%s\n", compressor, cspeed, dspeed, nbytes, cbytes, ratio, o.filename);


    /* Free resources */
    blosc2_schunk_free(schunk);
    blosc_destroy();

    free(data);
    free(data_dest);

    return EXIT_SUCCESS;
}

