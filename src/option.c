#include "option.h"

int option_usage() {
    fprintf(stderr, "Usage: ./main.exe filename compressor\n");
    fprintf(stderr, "List of available compressors: ");
    for(int i = 0; i < nb_codecs; ++i) {
        fprintf(stderr, "%s, ", codecs[i]);
    }
    fprintf(stderr, "\n");
    fprintf(stderr, " [ -c CHUNK_LENGTH ] -- Chunk length [default: 1048576]\n");
    fprintf(stderr, " [ -l LEVEL ] -- Compression level [default: 1]\n");
    fprintf(stderr, " [ -p THREAD ] -- Number of threads [default: 8]\n");
    fprintf(stderr, " [ -b ] -- Use bitshuffle\n");
    fprintf(stderr, " [ -s ] -- Use shuffle\n");
    fprintf(stderr, " [ -o OUTDIR ] -- Output directory [default: /tmp]\n");
    exit(0);
}

option option_get(int argc, char* argv[argc+1]) {
        /* Default option values */
    option o = {
        .bitshuffle_flag = OPTION_BITSHUFFLE_FLAG,
        .chunk_len = OPTION_CHUNK_LEN,
        .level = OPTION_LEVEL,
        .nthreads = OPTION_NTHREADS,
        .outdir = OPTION_OUTDIR,
        .shuffle_flag = OPTION_SHUFFLE_FLAG,
    };

    int index, opt;
    while ((opt = getopt(argc, argv, "bc:hl:o:p:s")) != -1)
        switch (opt) {
            case 'b': o.bitshuffle_flag = true;
                      break;
            case 'c': o.chunk_len = atoi(optarg);
                      break;
            case 'h': option_usage();
                      exit(0);
            case 'l': o.level = atoi(optarg);
                      break;
            case 'o': strcpy(o.outdir, optarg);
                      break;
            case 'p': o.nthreads = atoi(optarg);
                      break;
            case 's': o.shuffle_flag = true;
                      break;
            case '?':
                      if (optopt == 'c')
                          fprintf(stderr, "Option -c requires an argument.\n");
                      else if (optopt == 'l')
                          fprintf(stderr, "Option -l requires an argument.\n");
                      else if (optopt == 'o')
                          fprintf(stderr, "Option -o requires an argument.\n");
                      else if (optopt == 'p')
                          fprintf(stderr, "Option -p requires an argument.\n");
                      else if (isprint(optopt))
                          fprintf(stderr, "Unknown option `-%c'.\n", optopt);
                      else
                          fprintf(stderr, "Unknown option character `\\x%x'.\n", optopt);
                       exit(EXIT_FAILURE);
            default: abort();
        }

    debug_print("chunk_len = %d, level = %d, nthreads = %d, outdir = %s, bitshuffle_flag = %d, shuffle_flag = %d\n",
            o.chunk_len, o.level, o.nthreads, o.outdir, o.bitshuffle_flag, o.shuffle_flag);

    debug_print("# args = %d, optind = %d\n", argc, optind);
    for (index = optind; index < argc; ++index) {
        debug_print("Non-option argument %s\n", argv[index]);
    }


    if (o.bitshuffle_flag && o.shuffle_flag) {
        fprintf(stderr, "bit-shuffle and byte-shuffle options cannot be activated jointly!\n");
        exit(EXIT_FAILURE);
    }

    if ((argc - optind) != 2) {
        option_usage();
    }

    char* filename = argv[optind];
    if (!filename || !file_exists(filename))
        exit(EXIT_FAILURE);

    char* codec = argv[optind+1];
    if (!codec || !codec_exists(codec))
        exit(EXIT_FAILURE);

    o.filename = filename;
    o.codec = codec;

    return o;
}