#pragma once

#include "common.h"
#include "codec.h"
#include "config.h"
#include "utils.h"

typedef struct option option;

struct option {
    /* Mandatory */
    char* filename;
    char* codec;
    /* Optional */
    bool bitshuffle_flag;
    int chunk_len;
    int level;
    int nthreads;
    char outdir[PATH_MAX];
    bool shuffle_flag;
};

int option_usage();
option option_get(int, char**);