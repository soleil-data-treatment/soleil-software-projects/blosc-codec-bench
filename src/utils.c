#include "utils.h"

bool file_exists(char* filename) {
    bool found = false;
    if (access(filename, F_OK) == 0) {
        found = true;
    } else {
        fprintf(stderr, "File does not exist: %s\n", filename);
    }
    return found;
}

long get_binfile_size(FILE* fp) {
    fseek(fp, 0L, SEEK_END);
    long file_size = ftell(fp);
    rewind(fp);
    return file_size;
}